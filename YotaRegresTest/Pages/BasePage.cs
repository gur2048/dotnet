﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using YotaRegresTest.Utils;

namespace YotaRegresTest.Pages
{
    public abstract class BasePage
    {
        protected IWebDriver driver;
        protected WebDriverWait wait;
        protected String Url;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, new TimeSpan(5));
        }

        public T OpenPage<T>() where T : BasePage
        {
            driver.Navigate().GoToUrl(Url);
            return (T) this;
        }

        public T ClickElement<T>(IWebElement element) where T : BasePage
        {
            try
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(element)).Click();
                return (T) this;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public T ClickElement<T>(By by) where T : BasePage
        {
            try
            {
                if (driver.HasElement(by))
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(by)).Click();
                    return (T) this;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public T clickElementBySendKey<T>(IWebElement element) where T : BasePage
        {
            try
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(element)).SendKeys(Keys.Enter);
                return (T) this;
            }
            catch (Exception e)
            {
                Console.WriteLine("Page isn't conteins this element: " + e);
                return null;
            }
        }

        public T clickElementBySendKey<T>(By by) where T : BasePage
        {
            try
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(by)).SendKeys(Keys.Enter);
                return (T) this;
            }
            catch (Exception e)
            {
                Console.WriteLine("Page isn't conteins this element: " + e);
                return null;
            }
        }

        public T typeInElement<T>(By by, String type) where T : BasePage
        {
            try
            {
                if (driver.HasElement(by))
                {
                    IWebElement element = wait.Until(ExpectedConditions.ElementIsVisible(by));
                    element.Click();
                    element.Clear();
                    element.SendKeys(type);
                    //driver.TypeDelay(by, type);
                    System.Threading.Thread.Sleep(1000);
                    return (T) this;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}


﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using YotaRegresTest.Pages;
using YotaRegresTest.Utils;

namespace YotaRegresTest
{
    [TestFixture]
    public class AbstractTest
    {
        protected RemoteWebDriver driver;
        protected MainPage mainPage;
        protected ChromeOptions options;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {    
            options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            driver = new ChromeDriver(options);
            mainPage = new MainPage(driver);
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
                driver.Quit();
        }
    }
}
